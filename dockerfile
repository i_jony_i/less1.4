FROM debian:9 as build

RUN apt update && apt install -y wget gcc make libpcre++-dev
RUN wget https://github.com/openresty/luajit2/archive/v2.1-20201229.tar.gz && tar xfvz v2.1-20201229.tar.gz && cd luajit2-2.1-20201229 && make && make install
RUN wget https://github.com/openresty/lua-resty-core/archive/v0.1.21.tar.gz && tar xfvz v0.1.21.tar.gz
RUN wget https://github.com/openresty/lua-resty-lrucache/archive/v0.10.tar.gz && tar xfvz v0.10.tar.gz && cp -r lua-resty-lrucache-0.10/lib lua-resty-core-0.1.21/
RUN wget http://nginx.org/download/nginx-1.19.3.tar.gz && tar xvfz nginx-1.19.3.tar.gz
RUN wget https://github.com/vision5/ngx_devel_kit/archive/v0.3.1.tar.gz && wget https://github.com/openresty/lua-nginx-module/archive/v0.10.19.tar.gz && tar xvfz v0.3.1.tar.gz && tar xvfz v0.10.19.tar.gz
RUN cd nginx-1.19.3 && export LUAJIT_LIB=/usr/local/lib && export LUAJIT_INC=../luajit2-2.1-20201229/src &&  ./configure  --without-http_gzip_module  --with-ld-opt="-Wl,-rpath,/usr/lib" --add-module=../ngx_devel_kit-0.3.1 --add-module=../lua-nginx-module-0.10.19 && make && make install
RUN ls -l /
 
FROM debian:9
RUN apt update && apt install -y make
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
COPY --from=build /usr/local/lib/libluajit-5.1.so /lib/x86_64-linux-gnu/libluajit-5.1.so.2 
COPY --from=build /lua-resty-core-0.1.21 /lua-resty-core
RUN cd /lua-resty-core && make install
RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx
CMD ["./nginx", "-g", "daemon off;"]
